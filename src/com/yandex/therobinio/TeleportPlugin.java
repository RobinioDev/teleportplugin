/*
The Main Class
 */
package com.yandex.therobinio;

import org.bukkit.plugin.java.JavaPlugin;

public class TeleportPlugin extends JavaPlugin{
    
    private TeleportPluginCommandExecutor cmdExecutor;
    
    @Override
    public void onEnable() {
        loadConfig();
        //implementing the CommandExecutor
        cmdExecutor = new TeleportPluginCommandExecutor(this);
        getCommand("tpa").setExecutor(cmdExecutor);
        getCommand("tpaccept").setExecutor(cmdExecutor);
        getCommand("tpdeny").setExecutor(cmdExecutor);
    }
    
    @Override
    public void onDisable() {
        
    }
    
    public void loadConfig() {
        getConfig().options().copyDefaults(true);
        saveConfig();
    }
}

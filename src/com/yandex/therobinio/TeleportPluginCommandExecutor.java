package com.yandex.therobinio;

import java.util.HashMap;
import java.util.Map;
import static org.bukkit.Bukkit.getServer;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TeleportPluginCommandExecutor implements CommandExecutor{
    
    private TeleportPlugin plugin;
    private Map<String,String> teleporter = new HashMap<String,String>();
    
    public TeleportPluginCommandExecutor(TeleportPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] args) {
        Player pa = null;
        if (cs instanceof Player) {
            pa = (Player) cs;
        }
        
        if (cmnd.getName().equalsIgnoreCase("tpa")) {
            if (args.length > 0) {
                if (args[0].equalsIgnoreCase("help")) {
                    cs.sendMessage(ChatColor.BLUE + "/tpa Sende eine Teleport Anfrage an einen Mitspieler!");
                    return true;
                }
                if (!(pa == null)) {
                    if (!(pa.hasPermission("tp.tpa"))) {
                    pa.sendMessage("Du hast nicht die erforderlichen Berechtigungen!");
                    return true;
                    }
                    if (!(pa.getName().equalsIgnoreCase(args[0]))) {
                        final Player pb = getServer().getPlayer(args[0]);
                        if (!(pb == null)) {
                            long replyTime = plugin.getConfig().getLong("requestTime")*20;
                            pa.sendMessage(ChatColor.GREEN + "Eine Anfrage wurde an " + ChatColor.YELLOW + pb.getName() + ChatColor.GREEN + " geschickt.");
                            pb.sendMessage(ChatColor.GREEN + "Nehme die Anfrage mit /tpaccept an oder lehne mit /tpdeny ab.");
                            //add the request to the table
                            teleporter.put(pb.getName(), pa.getName());
                            //automatic killing of the event
                            getServer().getScheduler().scheduleSyncDelayedTask(plugin, () -> {
                                endRequest(pb.getName());
                            }, replyTime);
                                return true;
                        }
                        cs.sendMessage(ChatColor.RED + "Dieser Spieler ist nicht online!");
                        return true;
                        
                    }
                    cs.sendMessage(ChatColor.RED + "Du kannst dich nicht zu dir selbst teleportieren!");
                    return true;
                    
                }
                cs.sendMessage(ChatColor.RED + "Nur Spieler können diesen Command benutzen");
                return true;
            }
            cs.sendMessage(ChatColor.RED + "/tpa <PlayerName>");
            return true;
        }
        
        if (cmnd.getName().equalsIgnoreCase("tpaccept")) {
            
            if (args[0].equalsIgnoreCase("help")) {
                cs.sendMessage(ChatColor.BLUE + "/tpaccept Nehme eine Teleport frage an!");
                return true;
            }
            //check for player
            if(!(pa == null)) {
                if (!(pa.hasPermission("tp.tpaccept"))) {
                    pa.sendMessage("Du hast nicht die erforderlichen Berechtigungen!");
                    return true;
                }
                if(teleporter.containsKey(pa.getName())) {
                    Player porter = getServer().getPlayer(teleporter.get(pa.getName()));
                    teleporter.remove(pa.getName());
                    if (!(porter == null)) {
                        porter.teleport(pa);
                        pa.sendMessage(ChatColor.GRAY + "Teleport erfolgreich!");
                        porter.sendMessage(ChatColor.GRAY + "Teleport erfolgreich!");
                        return true;
                    }
                    pa.sendMessage(ChatColor.RED + "Die Person ist leider nicht mehr Online!");
                    return true;
                    
                }
                pa.sendMessage(ChatColor.RED + "Du hast keine Teleport Anfrage!");
                return true;
                
            }
            pa.sendMessage(ChatColor.RED + "Nur Spieler können teleportieren!");
            return true;
            
        }    
        
        if (cmnd.getName().equalsIgnoreCase("tpdeny")) {
            if (args[0].equalsIgnoreCase("help")) {
                cs.sendMessage(ChatColor.BLUE + "/tpaccept Lehne eine Teleport frage ab!");
                return true;
            }
            //check for player
            if(!(pa == null)) {
                if (!(pa.hasPermission("tp.tpdeny"))) {
                    pa.sendMessage("Du hast nicht die erforderlichen Berechtigungen!");
                    return true;
                }
                if(teleporter.containsKey(pa.getName())) {
                    Player porter = getServer().getPlayer(teleporter.get(pa.getName()));
                    teleporter.remove(pa.getName());
                    if (!(porter == null)) {
                        pa.sendMessage(ChatColor.GRAY + "Erfolgreich abgelehnt!");
                        porter.sendMessage(ChatColor.RED + "Deine Anfrage wurde leider abgelehnt!");
                        return true;
                    }
                    pa.sendMessage(ChatColor.RED + "Die Person ist leider nicht mehr Online!");
                    return true;
                    
                }
                pa.sendMessage(ChatColor.RED + "Du hast keine Teleport Anfrage!");
                return true;
            }
            pa.sendMessage(ChatColor.RED + "Nur Spieler können teleportieren!");
            return true;
        } 
        return false;
    }
    
    public boolean endRequest(String key) {
        if (teleporter.containsKey(key)) {
            Player porter = getServer().getPlayer(teleporter.get(key));
            if (!(porter == null)) {
                porter.sendMessage("Deine Teleport Anfrage an " + key + " ist ausgelaufen");
            }
            teleporter.remove(key);
            return true;
        }
        return false;
    }
}
